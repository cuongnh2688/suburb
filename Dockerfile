#Build from base image is java 8
FROM java:8-jdk

# Get the path of file .jar in local directory
ARG JAR_FILE=target/suburb*.jar

# Create a working dir in docker
WORKDIR /opt/app

# Copy file .jar from local to the working dir in docker
# and rename file is user-service.jar
COPY ${JAR_FILE} suburb.jar

# define the command default. After container start, these commands are executed.
ENTRYPOINT ["java","-jar","suburb.jar"]

EXPOSE 8080