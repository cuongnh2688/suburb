package com.cmc.suburb.exception;

/**
 * The custom exception to handle the business exception in Application
 */
public class SuburbBusinessException extends Exception{

    public SuburbBusinessException(String errorMessage){
        super(errorMessage);
    }
}
