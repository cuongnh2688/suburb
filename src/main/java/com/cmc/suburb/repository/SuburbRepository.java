package com.cmc.suburb.repository;

import com.cmc.suburb.entity.Suburb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SuburbRepository extends JpaRepository<Suburb, Long> {

    /**
     * Get list of suburb by the range of post codes.
     * @param postCodes
     * @return list of suburb
     */
    List<Suburb> findByPostCodeIn(Set<String> postCodes);
}
