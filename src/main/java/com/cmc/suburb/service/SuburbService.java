package com.cmc.suburb.service;

import com.cmc.suburb.common.AppConstant;
import com.cmc.suburb.dto.SuburbDTO;
import com.cmc.suburb.entity.Suburb;
import com.cmc.suburb.exception.SuburbBusinessException;
import com.cmc.suburb.repository.SuburbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This service class handle all business relate the suburb info.
 */
@Service
public class SuburbService {

    @Autowired
    private SuburbRepository suburbRepository;

    /**
     * This method helps create the suburb info in database
     * @param suburbDTOs
     */
    public void createSuburbs(List<SuburbDTO> suburbDTOs) {

        // Prepare data for Suburb entity to persist.
        List<Suburb> suburbs = new ArrayList<Suburb>();
        for (SuburbDTO suburbDto : suburbDTOs) {
            suburbs.add(
                    Suburb.builder()
                            .postCode(suburbDto.getPostCode())
                            .suburbName(suburbDto.getSuburbName().toUpperCase())
                            .category(suburbDto.getCategory())
                            .build()
            );
        }

        // Persist data
        suburbRepository.saveAll(suburbs);
    }

    /**
     * This method check which post code of suburb is existed in data before persisting data.
     * @param postCodes
     * @return
     */
    public void checkExistingPostCode(List<String> postCodes) throws SuburbBusinessException {

        // Get the suburb entities from database by the range of post code.
        List<Suburb> suburbs = suburbRepository.findByPostCodeIn(new HashSet<>(postCodes));

        // If it has not any post code exist in database, it will throw new exception.
        if(!suburbs.isEmpty()) {
            StringBuilder existingPostCode = new StringBuilder();
            suburbs.forEach(suburb -> {
                        existingPostCode.append(suburb.getPostCode())
                                .append(AppConstant._SPACE);
                    });
            existingPostCode.append(AppConstant._ERROR_MESSAGE._EXISTING_IN_DATABASE);
            throw new SuburbBusinessException(existingPostCode.toString());
        }
    }

    /**
     * Get suburb data from data base.
     * @param postCodes
     * @return
     * @throws SuburbBusinessException
     */
    public List<SuburbDTO> getSuburbInfo(List<String> postCodes) throws SuburbBusinessException {

        // Get the suburb entities from database by the range of post code.
        List<Suburb> suburbs = suburbRepository.findByPostCodeIn(new HashSet(postCodes));

        // Check the list suburb entities is empty or not
        if(suburbs.isEmpty()) {
            throw new SuburbBusinessException(AppConstant._ERROR_MESSAGE._SUBURBS_NOT_FOUND);
        }

        // Get suburb data from the list suburb entities.
        List<SuburbDTO> suburbData = new ArrayList<SuburbDTO>();
        for (Suburb suburb : suburbs) {
            suburbData.add(SuburbDTO.builder()
                    .postCode(suburb.getPostCode())
                    .suburbName(suburb.getSuburbName())
                    .category(suburb.getCategory())
                    .build());
        }

        // Sort suburb data by suburb name.
        List<SuburbDTO> sortedSuburbData = suburbData.stream()
                .sorted(Comparator.comparing(SuburbDTO::getSuburbName))
                .collect(Collectors.toList());

        return sortedSuburbData;
    }
}
