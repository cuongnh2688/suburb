package com.cmc.suburb.log;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class HttpRequestResponseFilter implements Filter {

    public static final String USER_AGENT_HEADER = "User-Agent";
    public static final String METHOD = "method";
    public static final String PROTOCOL = "protocol";
    public static final String REMOTE_HOST = "remoteHost";
    public static final String REQUESTER_IP = "requesterIP";
    public static final String RESPONSE_CODE = "responseCode";
    public static final String USER_AGENT = "userAgent";
    public static final String USER_INFO = "Kaka";
    public static final String MESSAGE = "message";
    public static final String Brut = "brut";


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        this.clearMDC();
        filterChain.doFilter(servletRequest, servletResponse);
        this.insertIntoMDC(servletRequest, servletResponse);
        log.info("Filter...");
    }

    private void insertIntoMDC(ServletRequest request, ServletResponse response) {

        MDC.put(HttpRequestResponseFilter.REMOTE_HOST, request.getRemoteHost());

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            MDC.put(METHOD, httpServletRequest.getMethod());
            MDC.put(REQUESTER_IP, httpServletRequest.getRemoteAddr());
            MDC.put(USER_AGENT, httpServletRequest.getHeader(USER_AGENT_HEADER));
            MDC.put(PROTOCOL, httpServletRequest.getProtocol());
        }

        if (response instanceof HttpServletResponse) {
            MDC.put(RESPONSE_CODE, String.valueOf(((HttpServletResponse) response).getStatus()));
        }
    }

    private void clearMDC() {
        MDC.remove(REMOTE_HOST);
        MDC.remove(METHOD);
        MDC.remove(USER_AGENT);
        MDC.remove(RESPONSE_CODE);
        MDC.remove(PROTOCOL);
        MDC.remove(REQUESTER_IP);
    }

}
