package com.cmc.suburb.controller;

import com.cmc.suburb.common.AppConstant;
import com.cmc.suburb.common.ValidSuburbList;
import com.cmc.suburb.dto.SuburbDTO;
import com.cmc.suburb.exception.SuburbBusinessException;
import com.cmc.suburb.service.SuburbService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * There are REST APIS for executing the CRUD operation for suburb resource.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/suburb")
public class SuburbController {

    @Autowired
    private SuburbService suburbService;

    /**
     * This API for creating the suburb info in database.
     * @param suburbs
     * @return the HTTP CODE 200 with message if creating suburb info is successful.
     */
    @PostMapping("/create-suburbs")
    public ResponseEntity<String> createSuburbInfo(@RequestBody @Valid ValidSuburbList<SuburbDTO> suburbs) throws SuburbBusinessException {

        // Get the suburb info
        List<SuburbDTO> suburbDTOs = suburbs.getValues();

        // Check the duplication of post code
        this.validateDuplicatePostCodes(suburbDTOs);

        // Check the post codes are existed in database or not
        this.checkExistPostCodeInDatabase(suburbDTOs);

        // Call service to persist data into database
        this.suburbService.createSuburbs(suburbDTOs);

        return ResponseEntity.ok(AppConstant._INFO_MESSAGE._CREATE_SUBURB_INFO_SUCCESSFULLY);
    }

    /**
     * This API for get the suburb info from database by a range of post codes.
     * Example API URL: http://Address:port/api/v1/suburb/suburb-info/6001,6003,6843
     * @param postCodes
     * @return The list info of suburbs.
     */
    @GetMapping("/suburb-info/{postCodes}")
    public ResponseEntity<List<SuburbDTO>> getSuburbInfo(@PathVariable List<String> postCodes) throws SuburbBusinessException {
        return ResponseEntity.ok(this.suburbService.getSuburbInfo(postCodes));
    }

    /**
     * This method validate the post code in the list suburb objects is duplicate or not.
     * @param suburbDTOs
     * @throws SuburbBusinessException
     */
    private void validateDuplicatePostCodes(List<SuburbDTO> suburbDTOs) throws SuburbBusinessException {

        List<String> postCodes = new ArrayList<>();

        // Check duplicate post code
        StringBuilder errorMessage;
        String postCode;
        for (SuburbDTO suburb : suburbDTOs) {

            // Get Post Code from a suburb info
            postCode = suburb.getPostCode();

            // Check duplicate post code
            if (postCodes.contains(postCode)) {

                errorMessage = new StringBuilder();
                errorMessage.append(AppConstant._LABEL._POST_CODE)
                        .append(postCode)
                        .append(AppConstant._ERROR_MESSAGE._DUPLICATE_POST_CODE);

                throw new SuburbBusinessException(errorMessage.toString());
            }

            postCodes.add(postCode);
        }
    }

    /**
     * Check exist post code in database
     * @param suburbDTOs
     * @throws SuburbBusinessException
     */
    private void checkExistPostCodeInDatabase(List<SuburbDTO> suburbDTOs) throws SuburbBusinessException {
        List<String> postCodes = new ArrayList<String>();
        suburbDTOs.forEach( suburbDTO -> {
            postCodes.add(suburbDTO.getPostCode());
        });
        this.suburbService.checkExistingPostCode(postCodes);
    }
}
