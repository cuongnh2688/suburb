package com.cmc.suburb.dto;

import com.cmc.suburb.common.AppConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * This object store the suburb info get from request body.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuburbDTO {

    /**
     * Post code of suburb
     * Post code is not empty, and it is numeric and has 4 digits.
     */
    @NotEmpty(message = AppConstant._ERROR_MESSAGE._POSTCODE_NOT_EMPTY)
    @Pattern(regexp = AppConstant._REGEX_PATTERN._NUMERIC_AND_4_DIGITS_REGEX
            ,message = AppConstant._ERROR_MESSAGE._NUMERIC_AND_4_DIGITS)
    private String postCode;

    /* Suburb name. It is not empty */
    @NotEmpty(message = AppConstant._ERROR_MESSAGE._SUBURB_NOT_EMPTY)
    private String suburbName;

    /* Category of Suburb */
    private String category;
}
