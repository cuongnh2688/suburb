package com.cmc.suburb.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "SUBURB")
public class Suburb {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "POST_CODE", unique = true, nullable = false)
    private String postCode;

    @Column(name = "SUBURB_NAME", nullable = false)
    private String suburbName;

    @Column(name = "CATEGORY")
    private String category;
}
