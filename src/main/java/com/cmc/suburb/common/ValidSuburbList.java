package com.cmc.suburb.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.List;

/**
 * It is a custom valid list, which helps we validate a parameter is list object.
 * @param <E>
 */
public class ValidSuburbList<E> {

    @JsonValue
    @Valid
    @NotNull
    @Size(min = 1, message = AppConstant._ERROR_MESSAGE._AT_LEAST_ONE_ITEM_IN_LIST)
    private List<E> values;

    @JsonCreator
    public ValidSuburbList(E... items) {

        this.values = Arrays.asList(items);
    }

    public List<E> getValues() {
        return values;
    }

    public void setValues(List<E> values) {
        this.values = values;
    }
}
