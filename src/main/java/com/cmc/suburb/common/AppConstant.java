package com.cmc.suburb.common;

/**
 * This class defines all constant used in the Application.
 */
public interface AppConstant {

    public static String _COLON = ": ";
    public static String _SPACE = " ";

    /**
     * Define the labels which are using in Application.
     * It is better when using the i18n.
     */
    public interface _LABEL {
        public static String _ERROR_MESSAGE = "Error message";
        public static String _POST_CODE = "Post code ";
    }

    /**
     * The error messages for validating the input parameters.
     * It is better when using the i18n.
     */
    public interface _ERROR_MESSAGE {
        public static String _NUMERIC_AND_4_DIGITS = "the Post Code is numeric and contain 4 digits!";
        public static String _POSTCODE_NOT_EMPTY = "The Post Code is not empty!";
        public static String _SUBURB_NOT_EMPTY = "The suburb name is not empty!";
        public static String _AT_LEAST_ONE_ITEM_IN_LIST ="Array body must contain at least one item!";
        public static String _SUBURBS_NOT_FOUND = "There aren't any suburbs with post codes in the list parameters";
        public static String _DUPLICATE_POST_CODE = " is duplicated";
        public static String _EXISTING_IN_DATABASE = "exist in database";
    }

    /**
     * The info messages for in application.
     * It is better when using the i18n.
     */
    public interface _INFO_MESSAGE {
        public static String _CREATE_SUBURB_INFO_SUCCESSFULLY = "The suburb info are created successfully!";
    }

    /**
     * Regex patterns which are used in application.
     */
    public interface _REGEX_PATTERN {

        /* Check an input string is numeric, and it's length is 4 */
        public static String _NUMERIC_AND_4_DIGITS_REGEX = "[\\d]{4}";
    }
}
